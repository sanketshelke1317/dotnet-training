﻿
using LowerStdLib;

#region first
//Console.WriteLine("Enter Product Details");
//Console.WriteLine("Enter Product Id");
//int productId = int.Parse(Console.ReadLine());
//Console.WriteLine("Enter Product Name");
//string productName = Console.ReadLine();
//Console.WriteLine("Enter Product Description");
//string productDescription = Console.ReadLine();

//Console.WriteLine("Product Details");

//Console.WriteLine($" Product Id : {productId} \n Product Name : {productName} \n Product Description : {productDescription}");

#endregion

#region library
//Class1 user1 = new Class1();
//user1.userName = "Sanket";
//Console.WriteLine(user1.getUsername());
#endregion

#region or and 
//Console.WriteLine("Enter Username");
//string userName = Console.ReadLine();
//Console.WriteLine("Enter Password");
//string password = Console.ReadLine();

//if(userName == "test" && password == "test")
//{
//    Console.WriteLine("Login Successful");
//}
//else if(userName == "test" && password != "test"){
//    Console.WriteLine("Password  incorrect");
//}    
//else if(password == "test" && userName != "test") 
//{
//    Console.WriteLine("UserName  incorrect");
//}
//else
//{
//    Console.WriteLine("UserName and Password are incorrect");
//}

#endregion

char A = default;
Console.WriteLine(A);